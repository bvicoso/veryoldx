# How do conservation and convergence influence the number of shared X-linked genes?

We simulate selection to convergently move genes onto the (different) X chromosomes of two related species. 

## Schematic of simulation 2

![alt text](images/Slide2.jpg)

## Control: observed/expected whith conservation only

We use the script described [here](FoldCutoffDamsel_v2.md), but with 1000 genes per chromosome, to make it comparable to the convergence simulations. The modified script is [here](Cons_Conv_ConservationFold.R)

## Code 1: Simulate gene movement - first randomly

A file containing all the code in one go, with bootstraps, is [here](onegoconsconv_v2.R)

* We start with 14 chromosomes with 1000 genes each. cx is the X in species 1, c2 is the X in species 2 (any two different chromosomes could have been picked).
* genes move independently in the two lineages with a probability that we set (and which corresponds to the percentage of genes that moved in each lineage at the end of the simulation) -- in this case we set it to 1 to fully reshuffle the genomic content before simulating convergent gene recruitment.
* gene movement is allowed onto all chromosomes (i.e. a gene can move within a chromosome), and the probability of landing on each chromosome is proportional to the number of genes on it. 


```
####Create pseudo-table of muller elements

#define data with genes and their muller elements
df <- data.frame(focal=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1),
                 guide=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1))
##pick which rows to duplicate
rows= c(1,2,3,4,5,6,7,8,9,10,11,12,13,14)
##add respective gene counts
#times = c(1688, 1451, 1387, 1115, 1158, 1209, 975, 1081, 961, 784, 796, 947, 442, 1036)
times = c(1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000)
allgenes<-df[rep(rows, times),]
rownames(allgenes) <- 1:nrow(allgenes)

## Select X-chromosome in each lineage (1=focal, 2=guide, 3=out, although these are interchangeable in this case)
## Pick different chromosomes for convergence, the same for conservation
X1<-"cx"
X2<-"c2"

## Chose percentage that moved (0 = no movement, 1 = full randomization)
moved<-1
## Estimate probability of moving to each chromosome
probabilities = times/sum(times)


##Go through table and add gene movement

##New table for results
movedtable<-as.data.frame(allgenes[1,])

for (i in 1:nrow(allgenes)) {

####Pick chromosomes in case we have gene movement (move 1 for focal, move2 for guide, move3 for outgroup)
move1<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)
move2<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)

####Open row of table
temp<-allgenes[i,]

###Randomly pick number, and based on probability decide movement or not
testnum1<-sample(1:100, 1, replace=FALSE)
testnum2<-sample(1:100, 1, replace=FALSE)


###If there is movement in guide
if (testnum2<(moved*100)) {
temp[2]<-move2
}
else {
}
###If there is movement in focal
if (testnum1<(moved*100)) {
temp[1]<-move1
}
else {
}


###Append results to new table
movedtable<-rbind(movedtable, temp)

}


# Remove first line that was added just to make table
movedtable<-movedtable[-1,]
```

## Code 2: Simulate gene movement - from autosomes to X

In this second step, genes move only into the X-chromosome of each species:
- we pick the probability that each gene is favored on the X (variable "beneficial" below).
- We also pick the probability that, if a gene is favored on the X, that it actually moves to the X (variable "movedgood" below). This probability applies to each lineage independently (i.e. if a gene is X-favored, it will have the same probability p of moving to the X in the two lineages)

```

## Chose percentage of genes that are beneficial to have on X
beneficial<-0.1

## Chose percentage of beneficial that moved onto X
movedgood<-0.1

##Go through table and add gene movement

##New table for results
selectedtable<-as.data.frame(movedtable[1,])

for (i in 1:nrow(allgenes)) {
####Open row of table
temp<-movedtable[i,]

###Randomly pick number, and based on probability decide movement or not
selectedgene<-sample(1:100, 1, replace=FALSE)
testnum1<-sample(1:100, 1, replace=FALSE)
testnum2<-sample(1:100, 1, replace=FALSE)

###If there is movement in guide
if (selectedgene<(beneficial*100) & testnum2<(movedgood*100)) {
temp[2]<-X2
}
else {
}
###If there is movement in focal
if (selectedgene<(beneficial*100) & testnum1<(movedgood*100)) {
temp[1]<-X1
}
else {
}


###Append results to new table
selectedtable<-rbind(selectedtable, temp)

}
# Remove first line that was added just to make table
selectedtable<-selectedtable[-1,]
```


## Code 3: Get stats

```
###stats

table(movedtable[,1])
table(movedtable[,2])

# Expected and observed sharing
 exp_shared12<-( table(movedtable[,1])[1]/sum(table(movedtable[,1])) ) * ( table(movedtable[,2])[1]/sum(table(movedtable[,2])) ) * sum(table(movedtable[,1]))

obs_shared_focgui<-dim(subset(movedtable, focal==X1 & guide==X2))[1]
obs_shared_focgui


##Summary
paste(moved, exp_shared12, obs_shared_focgui, sep=" ")

table(selectedtable[,1])
table(selectedtable[,2])

# Expected and observed sharing
 exp_shared_12<-( table(selectedtable[,1])[1]/sum(table(selectedtable[,1])) ) * ( table(selectedtable[,2])[2]/sum(table(selectedtable[,2])) ) * sum(table(selectedtable[,1]))

obs_shared_focgui<-dim(subset(selectedtable, focal==X1 & guide==X2))[1]
obs_shared_focgui


##Summary
paste(moved, beneficial, movedgood, exp_shared_12, obs_shared_focgui, sep=" ")
```




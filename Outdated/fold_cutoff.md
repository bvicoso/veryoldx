# How does the fold-enrichment change as genes move around?

## Simulate gene movement

* we assume no out-of-X effect
* element A is the X

```
####Create pseudo-table of muller elements

#define data
df <- data.frame(focal=rep(c('A', 'B', 'C', 'D', 'E', 'F'), each=1),
                 guide=rep(c('A', 'B', 'C', 'D', 'E', 'F'), each=1),
                 out=rep(c('A', 'B', 'C', 'D', 'E', 'F'), each=1))
##pick which rows to duplicate
rows= c(1,2,3,4,5,6)
##
##add respective gene counts
times = c(1000,1000,1000,1000,1000,100)
probabilities = times/sum(times)
allgenes<-df[rep(rows, times),]
rownames(allgenes) <- 1:nrow(allgenes)
## Chose percentage that moved
moved<-0.1

## Chose bias of out-of-X (p_out_of_x/p_other)
chrX<-"A"
px<-1

##Go through table and add gene movement

##New table for results
movedtable<-as.data.frame(allgenes[1,])

for (i in 1:nrow(allgenes)) {

####Pick chromosomes in case we have gene movement (move 1 for focal, move2 for guide, move3 for outgroup)
move1<-sample(c("A", "B", "C", "D", "E", "F"), size = 1, prob=probabilities)
move2<-sample(c("A", "B", "C", "D", "E", "F"), size = 1, prob=probabilities)
move3<-sample(c("A", "B", "C", "D", "E", "F"), size = 1, prob=probabilities)

####Open row of table
temp<-allgenes[i,]

###Randomly pick number, and based on probability decide movement or not
testnum1<-sample(1:100, 1, replace=FALSE)
testnum2<-sample(1:100, 1, replace=FALSE)
testnum3<-sample(1:100, 1, replace=FALSE)

###If the gene is X-linked in focal and there is movement
if (temp[1]==chrX && testnum1<(moved*px*100)) {
temp[1]<-move1
print("Xlinked")
print(temp[1])
} 
else {
}
###If the gene is not X-linked in focal and there is movement
if (temp[1]!=chrX && testnum1<(moved*100)) {
temp[1]<-move1
print("Not Xlinked")
print(temp[1])
}
else {
}
###If there is movement in guide
if (testnum2<(moved*100)) {
temp[2]<-move2
print("guide movement!")
print(temp[2])
}
else {
}
###If there is movement in outgroup
if (testnum3<(moved*100)) {
temp[3]<-move3
print("outgroup movement!")
print(temp[3])
}
else {
}
###Append results to new table
movedtable<-rbind(movedtable, temp)

}
```

## Get stats

```
###NEw stats

#How many / what percentage of genes moved?
dim(subset(movedtable, focal!=guide))[1]
# percentage
dim(subset(movedtable, focal!=guide))[1]/dim(movedtable)[1]
table(movedtable[,1])
table(movedtable[,2])

exp_shared<-( table(movedtable[,1])[1]/sum(table(movedtable[,1])) ) * ( table(movedtable[,2])[1]/sum(table(movedtable[,2])) ) * sum(table(movedtable[,1]))
obs_shared<-dim(subset(movedtable, focal=="A" & guide=="A"))[1]

exp_shared
obs_shared 
obs_shared/exp_shared
```


## Get stats for X=F

```
###NEw stats

#How many / what percentage of genes moved?
dim(subset(movedtable, focal!=guide))[1]
# percentage
dim(subset(movedtable, focal!=guide))[1]/dim(movedtable)[1]
table(movedtable[,1])
table(movedtable[,2])

exp_shared<-( table(movedtable[,1])[6]/sum(table(movedtable[,1])) ) * ( table(movedtable[,2])[6]/sum(table(movedtable[,2])) ) * sum(table(movedtable[,1]))
obs_shared<-dim(subset(movedtable, focal=="F" & guide=="F"))[1]

exp_shared
obs_shared 
obs_shared/exp_shared
```

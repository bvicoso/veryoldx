# How do conservation and convergence influence the number of shared X-linked genes?

[script in one go here](https://git.ista.ac.at/bvicoso/veryoldx/-/blob/main/onegoconsconv.txt)

## Simulate gene movement - first randomly

* genes move independently in the two lineages with a probability that we set (and which corresponds to the percentage of genes that moved in each lineage at the end of the simulation)
* gene movement is allowed onto all chromosomes (i.e. a gene can move within a chromosome), and the probability of landing on each chromosome is proportional to the number of genes on it. 


```
####Create pseudo-table of muller elements

#define data with genes and their muller elements
df <- data.frame(focal=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1),
                 guide=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1), out=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1))
##pick which rows to duplicate
rows= c(1,2,3,4,5,6,7,8,9,10,11,12,13,14)
##add respective gene counts
#times = c(1688, 1451, 1387, 1115, 1158, 1209, 975, 1081, 961, 784, 796, 947, 442, 1036)
times = c(1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000)
allgenes<-df[rep(rows, times),]
rownames(allgenes) <- 1:nrow(allgenes)

## Select X-chromosome in each lineage (1=focal, 2=guide, 3=out, although these are interchangeable in this case)
## Pick different chromosomes for convergence, the same for conservation
X1<-"cx"
X2<-"c2"
X3<-"c7"

## Chose percentage that moved (0 = no movement, 1 = full randomization)
moved<-1
## Estimate probability of moving to each chromosome
probabilities = times/sum(times)


##Go through table and add gene movement

##New table for results
movedtable<-as.data.frame(allgenes[1,])

for (i in 1:nrow(allgenes)) {

####Pick chromosomes in case we have gene movement (move 1 for focal, move2 for guide, move3 for outgroup)
move1<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)
move2<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)
move3<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)

####Open row of table
temp<-allgenes[i,]

###Randomly pick number, and based on probability decide movement or not
testnum1<-sample(1:100, 1, replace=FALSE)
testnum2<-sample(1:100, 1, replace=FALSE)
testnum3<-sample(1:100, 1, replace=FALSE)


###If there is movement in guide
if (testnum2<(moved*100)) {
temp[2]<-move2
}
else {
}
###If there is movement in focal
if (testnum1<(moved*100)) {
temp[1]<-move1
}
else {
}

###If there is movement in out
if (testnum3<(moved*100)) {
temp[3]<-move3
}
else {
}

###Append results to new table
movedtable<-rbind(movedtable, temp)

}


# Remove first line that was added just to make table
movedtable<-movedtable[-1,]
```

## Simulate gene movement - from autosomes to X

In this second step, genes move only into the X-chromosome of each species:
- we pick the probability that each gene is favored on the X 
- We also pick the probability that, if a gene is favored on the X, that it actually moves to the X. This probability applies to each lineage independently (i.e. if a gene is X-favored, it will have the same probability p of moving to the X in the three lineages)

```

## Chose percentage of genes that are beneficial to have on X
beneficial<-0.1

## Chose percentage of beneficial that moved onto X
movedgood<-0.1

##Go through table and add gene movement

##New table for results
selectedtable<-as.data.frame(movedtable[1,])

for (i in 1:nrow(allgenes)) {
####Open row of table
temp<-movedtable[i,]

###Randomly pick number, and based on probability decide movement or not
selectedgene<-sample(1:100, 1, replace=FALSE)
testnum1<-sample(1:100, 1, replace=FALSE)
testnum2<-sample(1:100, 1, replace=FALSE)
testnum3<-sample(1:100, 1, replace=FALSE)

###If there is movement in guide
if (selectedgene<(beneficial*100) & testnum2<(movedgood*100)) {
temp[2]<-X2
}
else {
}
###If there is movement in focal
if (selectedgene<(beneficial*100) & testnum1<(movedgood*100)) {
temp[1]<-X1
}
else {
}

###If there is movement in out
if (selectedgene<(beneficial*100) & testnum3<(movedgood*100)) {
temp[3]<-X3
}
else {
}

###Append results to new table
selectedtable<-rbind(selectedtable, temp)

}
# Remove first line that was added just to make table
selectedtable<-selectedtable[-1,]
```


## Get stats

```
###stats

table(movedtable[,1])
table(movedtable[,2])
table(movedtable[,3])
# Expected and observed sharing
 exp_shared12<-( table(movedtable[,1])[1]/sum(table(movedtable[,1])) ) * ( table(movedtable[,2])[1]/sum(table(movedtable[,2])) ) * sum(table(movedtable[,1]))
 exp_shared123<-( table(movedtable[,1])[1]/sum(table(movedtable[,1])) ) * ( table(movedtable[,2])[1]/sum(table(movedtable[,2])) )  * ( table(movedtable[,3])[1]/sum(table(movedtable[,3])) )* sum(table(movedtable[,1]))
obs_shared_focgui<-dim(subset(movedtable, focal==X1 & guide==X2))[1]
obs_shared_focout<-dim(subset(movedtable, focal==X1 & out==X3))[1]
obs_shared_focguiout<-dim(subset(movedtable, focal==X1 & guide==X2 & out==X3))[1]
obs_shared_focgui
obs_shared_focout
obs_shared_focguiout

##Summary
paste(moved, exp_shared12, obs_shared_focgui, obs_shared_focout, exp_shared123, obs_shared_focguiout, sep=" ")

table(selectedtable[,1])
table(selectedtable[,2])
table(selectedtable[,3])

# Expected and observed sharing
 exp_shared_12<-( table(selectedtable[,1])[1]/sum(table(selectedtable[,1])) ) * ( table(selectedtable[,2])[2]/sum(table(selectedtable[,2])) ) * sum(table(selectedtable[,1]))
 exp_shared_123<-( table(selectedtable[,1])[1]/sum(table(selectedtable[,1])) ) * ( table(selectedtable[,2])[2]/sum(table(selectedtable[,2])) )  * ( table(selectedtable[,3])[3]/sum(table(selectedtable[,3])) ) * sum(table(selectedtable[,1]))
obs_shared_focgui<-dim(subset(selectedtable, focal==X1 & guide==X2))[1]
obs_shared_focout<-dim(subset(selectedtable, focal==X1 & out==X3))[1]
obs_shared_focguiout<-dim(subset(selectedtable, focal==X1 & guide==X2 & out==X3))[1]
obs_shared_focgui
obs_shared_focout
obs_shared_focguiout

##Summary
paste(moved, beneficial, movedgood, exp_shared_12, obs_shared_focgui, obs_shared_focout, exp_shared_123, obs_shared_focguiout, sep=" ")
```



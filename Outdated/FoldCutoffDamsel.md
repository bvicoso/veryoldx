# How does the fold-enrichment change as genes move around?

## Simulate gene movement

* we assume no out-of-X effect
* element cx is the X
* we simulate three species because it will come in handy for other questions, but for now ignore the third (out)

```
####Create pseudo-table of muller elements

#define data
df <- data.frame(focal=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1),
                 guide=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1),
                 out=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1))
##pick which rows to duplicate
rows= c(1,2,3,4,5,6,7,8,9,10,11,12,13,14)
##
##add respective gene counts
times = c(1688, 1451, 1387, 1115, 1158, 1209, 975, 1081, 961, 784, 796, 947, 442, 1036)
probabilities = times/sum(times)
allgenes<-df[rep(rows, times),]
rownames(allgenes) <- 1:nrow(allgenes)
## Chose percentage that moved
moved<-0.1

## Chose bias of out-of-X (p_out_of_x/p_other)
chrX<-"cx"
px<-1

##Go through table and add gene movement

##New table for results
movedtable<-as.data.frame(allgenes[1,])

for (i in 1:nrow(allgenes)) {

####Pick chromosomes in case we have gene movement (move 1 for focal, move2 for guide, move3 for outgroup)
move1<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)
move2<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)
move3<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)

####Open row of table
temp<-allgenes[i,]

###Randomly pick number, and based on probability decide movement or not
testnum1<-sample(1:100, 1, replace=FALSE)
testnum2<-sample(1:100, 1, replace=FALSE)
testnum3<-sample(1:100, 1, replace=FALSE)

###If the gene is X-linked in focal and there is movement
if (temp[1]==chrX && testnum1<(moved*px*100)) {
temp[1]<-move1
#print("Xlinked")
#print(temp[1])
} 
else {
}
###If the gene is not X-linked in focal and there is movement
if (temp[1]!=chrX && testnum1<(moved*100)) {
temp[1]<-move1
#print("Not Xlinked")
#print(temp[1])
}
else {
}
###If there is movement in guide
if (testnum2<(moved*100)) {
temp[2]<-move2
#print("guide movement!")
#print(temp[2])
}
else {
}
###If there is movement in outgroup
if (testnum3<(moved*100)) {
temp[3]<-move3
#print("outgroup movement!")
#print(temp[3])
}
else {
}
###Append results to new table
movedtable<-rbind(movedtable, temp)

}
```

## Get stats

```
###NEw stats

#How many / what percentage of genes are on different elements?
tot_mov<- dim(subset(movedtable, focal!=guide))[1]
tot_mov

# percentage of genes now on different elements
prop_mov<-dim(subset(movedtable, focal!=guide))[1]/dim(movedtable)[1]
prop_mov

table(movedtable[,1])
table(movedtable[,2])

# Expected and observed sharing
exp_shared<-( table(movedtable[,1])[14]/sum(table(movedtable[,1])) ) * ( table(movedtable[,2])[14]/sum(table(movedtable[,2])) ) * sum(table(movedtable[,1]))
obs_shared<-dim(subset(movedtable, focal=="cx" & guide=="cx"))[1]

exp_shared
obs_shared 
obs_shared/exp_shared

##Summary
paste(moved, tot_mov, prop_mov, exp_shared, obs_shared, obs_shared/exp_shared, sep=" ")
```

# Simulations for Toups & Vicoso (2023)

* [Simulation 1](FoldCutoffDamsel_v2.md): shared X-linked genes under conservation of the same X chromosome.

* [Simulation 2](cons_conv_V2.md): shared X-linked genes under recruitment of a subset of genes to two independent X chromosomes.

# How much enrichment of shared X-linked genes is expected as a larger proportion of genes move around the genome?

We simulate gene movement between a pair of species that share the same conserved X chromosome, and calculate the observed/expected ratio of shared X-linked genes between them after different amounts of random gene movement have occurred.

## Schematic of simulation

![alt text](images/Slide1.jpg)

## Code 1: Simulate gene movement

* We start with 13 autosomes and one X, as in damselfly, with their respective numbers of genes in this species.
* element cx is the X.
* genes move independently in the two lineages with a probability that we set (variable "moved" below, which also corresponds to the percentage of genes that moved in each lineage at the end of the simulation).
* gene movement is allowed onto all chromosomes (i.e. a gene can move within a chromosome), and the probability of landing on each chromosome is proportional to the number of genes on it. 

A file containing the code in one go, with bootstraps, is [here](https://git.ista.ac.at/bvicoso/veryoldx/-/blob/main/FoldCutOff_boot.R).

```
####Create pseudo-table of muller elements

#define data with genes and their muller elements
df <- data.frame(focal=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1),
                 guide=rep(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), each=1))
##pick which rows to duplicate
rows= c(1,2,3,4,5,6,7,8,9,10,11,12,13,14)
##add respective gene counts
times = c(1688, 1451, 1387, 1115, 1158, 1209, 975, 1081, 961, 784, 796, 947, 442, 1036)
allgenes<-df[rep(rows, times),]
rownames(allgenes) <- 1:nrow(allgenes)

## Chose percentage that moved
moved<-0.1
## Estimate probability of moving to each chromosome
probabilities = times/sum(times)


##Go through table and add gene movement

##New table for results
movedtable<-as.data.frame(allgenes[1,])

for (i in 1:nrow(allgenes)) {

####Pick chromosomes in case we have gene movement (move 1 for focal, move2 for guide, move3 for outgroup)
move1<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)
move2<-sample(c('c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'cx'), size = 1, prob=probabilities)

####Open row of table
temp<-allgenes[i,]

###Randomly pick number, and based on probability decide movement or not
testnum1<-sample(1:100, 1, replace=FALSE)
testnum2<-sample(1:100, 1, replace=FALSE)


###If there is movement in guide
if (testnum2<(moved*100)) {
temp[2]<-move2
}
else {
}
###If there is movement in focal
if (testnum1<(moved*100)) {
temp[1]<-move1
}
else {
}
###Append results to new table
movedtable<-rbind(movedtable, temp)

}


# Remove first line that was added just to make table
movedtable<-movedtable[-1,]
```

## Code 2: Get stats

```
###stats

#How many / what percentage of genes are on different elements after gene movement?
tot_mov<- dim(subset(movedtable, focal!=guide))[1]
tot_mov

# percentage of genes now on different elements after gene movement
prop_mov<-dim(subset(movedtable, focal!=guide))[1]/dim(movedtable)[1]
prop_mov

table(movedtable[,1])
table(movedtable[,2])

# Expected and observed sharing
exp_shared<-( table(movedtable[,1])[14]/sum(table(movedtable[,1])) ) * ( table(movedtable[,2])[14]/sum(table(movedtable[,2])) ) * sum(table(movedtable[,1]))
obs_shared<-dim(subset(movedtable, focal=="cx" & guide=="cx"))[1]

exp_shared
obs_shared 
obs_shared/exp_shared

##Summary
paste(moved, tot_mov, prop_mov, exp_shared, obs_shared, obs_shared/exp_shared, sep=" ")
```

